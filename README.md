# Awesome AS Widgets

Some widgets I've made for AwesomeWM.

If you are looking for more awesome widgets, I can highly recommend the ones at:

https://github.com/streetturtle/awesome-wm-widgets

## [NordVPN Widget](https://bitbucket.org/easlice/awesome-as-widgets/src/master/nordvpn/)

Displays the status of your NordVPN connection. Toggle it on or off by clicking
the icon. Mouse over it for a more detailed status. Icons are configurable.

 ![widget_screenshot](https://bitbucket.org/easlice/awesome-as-widgets/raw/7de3043a72e1de200ec39156be755f27262bfc95/screenshots/nordvpn-status.png)

## [Cheatsheet Widget](https://bitbucket.org/easlice/awesome-as-widgets/src/master/cheatsheet/)

### NOTE: Instead of using this, just hit `mod4 + s`. Still, I'll be leaving this up for posterity.

An icon that, when clicked on, displays a popup that has a number of the keyboard shortcuts for Awesome on it. The icon and font are configurable.

 ![widget_screenshot](https://bitbucket.org/easlice/awesome-as-widgets/raw/65ac1536cf6dd42c8fb227cdbf71d386f202457c/screenshots/cheatsheet.png)
