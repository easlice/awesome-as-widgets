# Keyboard Shortcut Cheatsheet Widget

### NOTE: Instead of using this, just hit `mod4 + s`. Still, I'll be leaving this up for posterity.

This widget, when clicked, displays a popup that lists a number of keyboard shortcuts for Awesome. Most of these are easy to remember, but I got tired of keeping a browser tab open to look up some of the only occasionally useful ones.

 ![widget_screenshot](https://bitbucket.org/easlice/awesome-as-widgets/raw/65ac1536cf6dd42c8fb227cdbf71d386f202457c/screenshots/cheatsheet.png)

## Features
- Clicking on the icon toggles the display of the popup.
- The icon is customizable.
- The icon is automatically recolored based on your themes fg_normal color.
- You can set the font used by the icon.
- You can adjust the screen offsets of the popup.
- The widget is an awful.widget.button, and can be joined with other buttons.

## Dependencies

By default it use an Arc icon, though that can be changed.

## Customization

| Name | Default | Description |
|---|---|---|
| `font` | `FreeMono` | The font to use. |
| `icon` | `/usr/share/icons/Arc/status/symbolic/dialog-question-symbolic.svg` | The path to the icon to use for the widget. |
| `offset` | `{ y = 5 }`| The offset applied to the popup when it apppears. |

## Installation

Then clone this repo under **~/.config/awesome/** and add the widget to your **rc.lua**:

```lua
local cheatsheet_widget = require("awesome-as-widgets.cheatsheet.button")
...
s.mytasklist, -- Middle widget
    { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        ...
        -- default
        cheatsheet_widget(),
        -- customized
        cheatsheet_widget({
            font = "Overpass",
            icon = "~/icons/clueless_fool.png",
            offset = { x = 19, y = 19 },
        }),
		...
```
