--------------------------------------------------------------------------------
-- Awesome Shortcut Cheatsheet Widget
-- Icon will display a cheatsheet of keyboard shortcuts when clicked.
-- More details can be found here:
-- https://bitbucket.org/easlice/awesome-as-widgets

-- @author Andrew Slice
-- @license MIT
--------------------------------------------------------------------------------
--
local awful     = require("awful")
local beautiful = require("beautiful")
local gears     = require("gears")
local wibox     = require("wibox")

local this = {}
local CHEATSHEET_MARKUP = [[
Mod4 + w                        : Open main menu.
Mod4 + m                        : Maximize client.
Mod4 + n                        : Minimize client.
Mod4 + Control + Shift + c      : Kill focused client.
Mod4 + r                        : 'Run' Prompt
Mod4 + Return                   : Spawn a Terminal
Mod4 + Escape                   : Focus previously selected tag set.
Mod4 + Button3 on tag name      : Toggle this tag for client.
Mod4 + Button1 on client window : Move window.
Mod4 + Button3 on client window : Resize window.
Mod4 + [Space/Shift + Space]    : Switch to next/previous layout.
Mod4 + [Left/Right]             : View previous/next tag.
Mod4 + [j/k]                    : Focus next/previous client.
Mod4 + Control + [j/k]          : Focus next/previous screen.
Mod4 + Shift + [j/k]            : Switch client with next/previous client.
Mod4 + o                        : Send client to next screen.
Mod4 + Control + Space          : Toggle client floating status.
Mod4 + Control + Return         : Swap focused client with master.
Mod4 + Control + 1-9            : Toggle tag view.
Mod4 + Shift + 1-9              : Tag client with tag.
Mod4 + Shift + Control + 1-9    : Toggle tag on client.
Mod4 + Control + r              : Restart Awesome
]]

local function worker(args)
    ----------------------------------------------------------------------------
    -- Initial Config
    local args = args or {}

    this.font   = args.font or "FreeMono"
    this.icon   = args.icon or "/usr/share/icons/Arc/status/symbolic/dialog-question-symbolic.svg"
    this.offset = args.offset or { y = 5 }
    ----------------------------------------------------------------------------

    ----------------------------------------------------------------------------
    -- Create the popup that will be displayed
    this.popup = awful.popup{
        ontop        = true,
        visible      = false,
        shape        = gears.shape.rounded_rect,
        border_width = 1,
        border_color = beautiful.bg_normal,
        offset       = this.offset,
        widget       = wibox.widget{
            markup = CHEATSHEET_MARKUP,
            align  = 'left',
            valign = 'center',
            font   = this.font,
            widget = wibox.widget.textbox
        }
    }
    ----------------------------------------------------------------------------

    ----------------------------------------------------------------------------
    -- Create the button to toggle the popup on and off
    this.button = awful.widget.button({
        image = gears.color.recolor_image(this.icon, beautiful.fg_normal)
    })

    this.button:buttons(gears.table.join(
        awful.button({}, 1, function()
            if this.popup.visible then
                this.popup.visible = false
            else
                this.popup:move_next_to(mouse.current_widget_geometry)
            end
        end)
    ))
    ----------------------------------------------------------------------------

    return this.button
end

return setmetatable(this, { __call = function(_, ...) return worker(...) end })
